require('dotenv/config')
const express = require('express')
const routes = require('./routes')
const session = require('express-session')
const cors = require('cors')
require('./database')

const app = express()

app.use(cors())
app.use(express.json())
app.use(session({
    secret: 'MyAppSecret',
    resave: false,
    saveUninitialized: false
}))
app.use(routes)

app.listen(process.env.PORT || 3333)