const Patient = require('../models/Patient')
const Attendance = require('../models/Attendance')

module.exports = {
    async index(req, res){
        const attendances = await Attendance.findAll()

        return res.json(attendances)
    },

    async show(req, res) {
        
        const attendance = await Attendance.findByPk(req.params.id)
        
        if (!attendance) {
            return res.status(400).json({ error: "Attendance not found" })
        }

        return res.json(attendance) 
    },

    async store(req, res) {
        const { patient_id } = req.params
        const { results, situation, responsible_unit } = req.body

        const patient = await Patient.findByPk(patient_id)

        if (!patient) {
            return res.status(400).json({ error: "Attendance not found" })
        }

        const attendance = await Attendance.create({
            results,
            situation,
            responsible_unit,
            patient_id,
        }).then(status => res.status(201).json({
            error: false,
            message: "Attendance has been created"
        })).catch(error => res.json({
            error: true,
            error: error
        }))    
    },

    async update(req, res, next) {
        try {
            const attendance = await Attendance.findByPk(req.params.id)

            await attendance.update(req.body)
            .then(status => res.status(201).json({
                error: false,
                message: "Attendance has been updated"
            }))  
        } catch (error) {
            return next(error)
        }
    },

    async delete(req, res) {
       
       const { id } = req.params       
       const attendance = await Attendance.findByPk(id)
     
       if (!attendance) {
        return res.status(400).json({ error: "Attendance not found" })
       }        
       
       await Attendance.destroy({ where: { id } }).then(status => res.status(201).json({
           error: false,
           message: "Attendance has been deleted"
       })).catch(error => res.json({
           error: true,
           error: error
       }))     
    }
   
}
