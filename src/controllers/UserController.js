const User = require('../models/User')

module.exports = {

    async index(req, res) {
        const users = await User.findAll()
        
        return res.json(users)
    },

    async show(req, res) {
        const { user_id } = req.params
        const user = await User.findByPk(user_id)
        
        if (!user) {
            return res.status(400).json({ error: "User not found" })
        }     
    },

    async store(req, res) {
        const { name, email, pass, active, level } = req.body

        await User.create({ name, email, pass, active, level })
        .then(status => res.status(201).json({
            error: false,
            message: "User has been created",
        })).catch(error => res.json({
            error: true,
            error: error
        }))          
    },

    async update(req, res, next) {
        try {
            const user = await User.findByPk(req.params.user_id)

            await user.update(req.body)
            .then(status => res.status(201).json({
                error: false,
                message: "User has been updated"
            }))  
        } catch (error) {
            return next(error)
        }
    },

    async delete(req, res) {
       
       const { user_id } = req.params       
       const user = await User.findByPk(user_id)

       if (!user) {
        return res.status(400).json({ error: "User not found" })
       }        
       
       await User.destroy({ where: { id: user_id } }).then(status => res.status(201).json({
           error: false,
           message: "User has been deleted"
       })).catch(error => res.json({
           error: true,
           error: error
       }))       
    }
}