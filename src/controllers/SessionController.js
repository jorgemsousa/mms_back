const  User  = require('../models/User')
const { response } = require('express')

class SessionController {

    async create (req, res) {
        return res.json({message: 'Usuário Logado'})
    }

    async store (req, res) {
      
        const { email, pass } = req.body
      
        const user = await User.findOne({ where: { email }})
             
        if(!user)
            return res.status(400).json({ error: 'User not found'})

        if (!await user.checkPassword(pass)){
            return res.status(400).json({ error: 'Invalid password' })
        }

        req.session.user = user

        return res.json( req.session.user )    
    }
    
}
    
module.exports = new SessionController
       
    