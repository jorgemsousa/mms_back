const Patient = require('../models/Patient')
const { show } = require('./UserController')

module.exports = {

    async index(req, res) {
        const patients = await Patient.findAll()
        
        return res.json(patients)
    },

    async show(req, res) {
        try {
            const { patient_id } = req.params

            const patient = await Patient.findByPk(patient_id)

            return res.json(patient)
        } catch (error) {
            return error
        }        
    },

    async store(req, res) {
       
             const { 
                 name, 
                 sex, 
                 street, 
                 number, 
                 complement, 
                 reference_point, 
                 neighborhood, 
                 zipcode, 
                 state, 
                 city, 
                 zone, 
                 mother_name 
            } = req.body
       
            await Patient.create({ name, sex, street, number, complement, reference_point, neighborhood, zipcode, state, city, zone, mother_name })
            .then(status => res.status(201).json({
                error: false,
                message: "Patient has been created"
            })).catch(error => res.json({
                error: true,
                error: error
            }))             
    },

    async update(req, res, next) {
        try {
            const patient = await Patient.findByPk(req.params.patient_id)

            await patient.update(req.body)
            .then(status => res.status(201).json({
                error: false,
                message: "Patient has been updated"
            }))  
        } catch (error) {
            return next(error)
        }
    },

    async delete(req, res) {
       
       const { patient_id } = req.params       
       const patient = await Patient.findByPk(patient_id)
     
       if (!patient) {
        return res.status(400).json({ error: "Patient not found" })
       }        
       
       await Patient.destroy({ where: { id: patient_id } }).then(status => res.status(201).json({
           error: false,
           message: "Patient has been deleted"
       })).catch(error => res.json({
           error: true,
           error: error
       }))     
    }
}