/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  const CadastroDomiciliar = sequelize.define('CadastroDomiciliar', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tb_usuario',
        key: 'id'
      }
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    uuidFichaOriginadora: {
      field: 'uuid_ficha_originadora',
      type: DataTypes.STRING,
      allowNull: true
    },
    inactive: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    lat: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    lng: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    microarea: {
      type: DataTypes.STRING,
      allowNull: true
    },
    stForaArea: {
      field: 'st_fora_area',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipoDeImovel: {
      field: 'tipo_imovel',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    numero: {
      type: DataTypes.STRING,
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    pontoReferencia: {
      field: 'ponto_referencia',
      type: DataTypes.STRING,
      allowNull: true
    },
    stSemNumero: {
      field: 'st_sem_numero',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    telelefoneResidencia: {
      field: 'telelefone_residencia',
      type: DataTypes.STRING,
      allowNull: true
    },
    telefoneContato: {
      field: 'telefone_contato',
      type: DataTypes.STRING,
      allowNull: true
    },
    situacaoMoradiaPosseTerra: {
      field: 'situacao_moradia_posse_terra',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    localizacao: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipoDomicilio: {
      field: 'tipo_domicilio',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nuMoradores: {
      field: 'qtd_moradores',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nuComodos: {
      field: 'qtd_comodos',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipoAcessoDomicilio: {
      field: 'tipo_acesso_domicilio',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    stDisponibilidadeEnergiaEletrica: {
      field: 'disponibilidade_energia_eletrica',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    condicaoPosseUsoTerra: {
      field: 'condicao_posse_uso_terra',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    materialPredominanteParedesExtDomicilio: {
      field: 'material_predominante_paredes_ext_domicilio',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    abastecimentoAgua: {
      field: 'abastecimento_agua',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    formaEscoamentoBanheiro: {
      field: 'forma_escoamento_banheiro',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    aguaConsumoDomicilio: {
      field: 'agua_consumo_domicilio',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    destinoLixo: {
      field: 'destino_lixo',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    stAnimaisNoDomicilio: {
      field: 'animais_domicilio',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING,
      allowNull: true
    },
    codigoibgemunicipio: {
      field: 'cod_ibge_municipio',
      type: DataTypes.STRING,
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING,
      allowNull: true
    },
    tipologradouronumerodne: {
      field: 'tipo_logradouro_numero_dne',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nomelogradouro: {
      field: 'nome_logradouro',
      type: DataTypes.STRING,
      allowNull: true
    },
    numerodneuf: {
      field: 'numero_dneuf',
      type: DataTypes.STRING,
      allowNull: true
    },
    nomeInstituicaoPermanencia: {
      field: 'nome_instituicao_permanencia',
      type: DataTypes.STRING,
      allowNull: true
    },
    nomeResponsavelTecnico: {
      field: 'nome_responsavel_tecnico',
      type: DataTypes.STRING,
      allowNull: true
    },
    cnsResponsavelTecnico: {
      field: 'cns_responsavel_tecnico',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cargoResponsavelTecnico: {
      field: 'cargo_responsavel_tecnico',
      type: DataTypes.STRING,
      allowNull: true
    },
    telefoneResponsavelTecnico: {
      field: 'telefone_responsavel_tecnico',
      type: DataTypes.STRING,
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING,
      allowNull: true
    },
    animaisGatos: {
      field: 'animais_gatos',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animaisCaes: {
      field: 'animais_caes',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animaisPassaros: {
      field: 'animais_passaros',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animaisOutros: {
      field: 'animais_outros',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    stOutrosProfissionaisVinculados: {
      field: 'outros_profissionais_vinculados',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    stPossuiPiscina: {
      field: 'possui_piscina',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    stExportado: {
      field: 'exportado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    fichaAtualizada: {
      field: 'ficha_atualizada',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTermoRecusa: {
      field: 'status_termo_recusa',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cbo_profissional: {
      type: DataTypes.STRING,
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      field: 'date_created',
      type: DataTypes.DATE
    },
    updatedAt: {
      field: 'date_updated',
      type: DataTypes.DATE
    }
  }, {
    tableName: 'tb_cadastro_domiciliar',
    timestamps: true
  });

  return CadastroDomiciliar;
};