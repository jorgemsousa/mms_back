module.exports = (sequelize, DataTypes) => {
  
  const CadastroIndividual = sequelize.define('CadastroIndividual', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tb_usuario',
        key: 'id'
      }
    },
    uuid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    uuidFichaOriginadora: {
      field: 'uuid_ficha_originadora',
      type: DataTypes.STRING,
      allowNull: true
    },
    inactive: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    data_cadastro: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    familia_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cnsCidadao: {
      field: 'cns_cidadao',
      type: DataTypes.BIGINT,
      allowNull: true
    },
    statusEhResponsavel: {
      field: 'status_responsavel',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cnsResponsavelFamiliar: {
      field: 'cns_responsavel_familiar',
      type: DataTypes.BIGINT,
      allowNull: true
    },
    microarea: {
      type: DataTypes.STRING,
      allowNull: true
    },
    stForaArea: {
      field: 'st_fora_area',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nomeCidadao: {
      field: 'nome_cidadao',
      type: DataTypes.STRING,
      allowNull: true
    },
    nomeSocial: {
      field: 'nome_social',
      type: DataTypes.STRING,
      allowNull: true
    },
    dataNascimentoCidadao: {
      field: 'data_nascimento',
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sexoCidadao: {
      field: 'sexo',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    racaCorCidadao: {
      field: 'raca_cor',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    etnia: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    numeroNisPisPasep: {
      field: 'nr_nis',
      type: DataTypes.STRING,
      allowNull: true
    },
    nomeMaeCidadao: {
      field: 'nome_mae',
      type: DataTypes.STRING,
      allowNull: true
    },
    desconheceNomeMae: {
      field: 'mae_desconhecida',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nomePaiCidadao: {
      field: 'nome_pai',
      type: DataTypes.STRING,
      allowNull: true
    },
    desconheceNomePai: {
      field: 'pai_desconhecido',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nacionalidadeCidadao: {
      field: 'nacionalidade',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    paisNascimento: {
      field: 'pais_nascimento',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    portariaNaturalizacao: {
      field: 'portaria_naturalizacao',
      type: DataTypes.STRING,
      allowNull: true
    },
    dtNaturalizacao: {
      field: 'dta_naturalizacao',
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    dtEntradaBrasil: {
      field: 'dta_entrada_brasil',
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    codigoIbgeMunicipioNascimento: {
      field: 'cod_ibge_munic_nascimento',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusTermoRecusaCadastroIndividualAtencaoBasica: {
      field: 'status_termo_recusa_cad_individual_atencao_basica',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    telefoneCelular: {
      field: 'telefone_celular',
      type: DataTypes.STRING,
      allowNull: true
    },
    emailCidadao: {
      field: 'email',
      type: DataTypes.STRING,
      allowNull: true
    },
    relacaoParentescoCidadao: {
      field: 'relacao_parentesco',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ocupacaoCodigoCbo2002: {
      field: 'ocupacao_cod_cbo2002',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusFrequentaEscola: {
      field: 'status_frequenta_escola',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    grauInstrucaoCidadao: {
      field: 'grau_instrucao',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    situacaoMercadoTrabalhoCidadao: {
      field: 'situacao_mercado_trabalho',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    responsavelPorCrianca: {
      field: 'responsavel_por_crianca',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusMembroPovoComunidadeTradicional: {
      field: 'status_membro_povo_comunidade_tradicional',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusFrequentaBenzedeira: {
      field: 'status_frequenta_benzedeira',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusParticipaGrupoComunitario: {
      field: 'status_participa_grupo_comunitario',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    povoComunidadeTradicional: {
      field: 'povo_comunidade_tradicional',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusPossuiPlanoSaudePrivado: {
      field: 'status_possui_plano_saude_privado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusDesejaInformarOrientacaoSexual: {
      field: 'status_deseja_informar_orientacao_sexual',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    orientacaoSexualCidadao: {
      field: 'orientacao_sexual',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusDesejaInformarIdentidadeGenero: {
      field: 'status_deseja_informar_ident_genero',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    identidadeGeneroCidadao: {
      field: 'identidade_genero',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusTemAlgumaDeficiencia: {
      field: 'status_tem_alguma_deficiencia',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    deficienciasCidadao: {
      field: 'deficiencias',
      type: DataTypes.STRING,
      allowNull: true
    },
    saidaCidadaoCadastro: {
      field: 'saida_cadastro',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    motivoSaidaCidadao: {
      field: 'motivo_saida',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    dataObito: {
      field: 'data_obito',
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    numeroDO: {
      field: 'numero_do',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusEhGestante: {
      field: 'status_gestante',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    maternidadeDeReferencia: {
      field: 'maternidade_referencia',
      type: DataTypes.STRING,
      allowNull: true
    },
    situacaoPeso: {
      field: 'situacao_peso',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusEhFumante: {
      field: 'status_fumante',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusEhDependenteAlcool: {
      field: 'status_dependente_alcool',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusEhDependenteOutrasDrogas: {
      field: 'status_dependente_outras_drogas',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemHipertensaoArterial: {
      field: 'status_tem_hipertensao_arterial',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemDiabetes: {
      field: 'status_tem_diabetes',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTeveAvcDerrame: {
      field: 'status_teve_avc_derrame',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTeveInfarto: {
      field: 'status_teve_infarto',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTeveDoencaCardiaca: {
      field: 'status_teve_doenca_cardiaca',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    doencaCardiaca: {
      field: 'doenca_cardiaca',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusTemTeveDoencasRins: {
      field: 'status_tem_teve_doencas_rins',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    doencaRins: {
      field: 'doenca_rins',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusTemDoencaRespiratoria: {
      field: 'status_tem_doenca_respiratoria',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    doencaRespiratoria: {
      field: 'doenca_respiratoria',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusTemHanseniase: {
      field: 'status_tem_hanseniase',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemTuberculose: {
      field: 'status_tem_tuberculose',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemTeveCancer: {
      field: 'status_tem_teve_cancer',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTeveInternadoem12Meses: {
      field: 'status_teve_internadoem_12meses',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    descricaoCausaInternacaoEm12Meses: {
      field: 'desc_causa_internacao_12meses',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusDiagnosticoMental: {
      field: 'status_diagnostico_mental',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusEstaAcamado: {
      field: 'status_acamado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusEstaDomiciliado: {
      field: 'status_domiciliado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusUsaPlantasMedicinais: {
      field: 'status_usa_plantas_medicinais',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    descricaoPlantasMedicinaisUsadas: {
      field: 'desc_plantas_medicinais_usadas',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusUsaOutrasPraticasIntegrativasOuComplementares: {
      field: 'status_usa_outras_praticas_integrativas_complem',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    descricaoOutraCondicao1: {
      field: 'desc_outra_condicao1',
      type: DataTypes.STRING,
      allowNull: true
    },
    descricaoOutraCondicao2: {
      field: 'desc_outra_condicao2',
      type: DataTypes.STRING,
      allowNull: true
    },
    descricaoOutraCondicao3: {
      field: 'desc_outra_condicao3',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusSituacaoRua: {
      field: 'status_situacao_rua',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tempoSituacaoRua: {
      field: 'tempo_situacao_rua',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusRecebeBeneficio: {
      field: 'status_recebe_ceneficio',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusPossuiReferenciaFamiliar: {
      field: 'status_possui_referencia_familiar',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    quantidadeAlimentacoesAoDiaSituacaoRua: {
      field: 'quant_refeicooes_diaria_rua',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    origemAlimentoSituacaoRua: {
      field: 'origem_alimento_situacao_rua',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusAcompanhadoPorOutraInstituicao: {
      field: 'status_acompanhado_por_outra_instituicao',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    outraInstituicaoQueAcompanha: {
      field: 'outra_instituicao_que_acompanha',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusVisitaFamiliarFrequentemente: {
      field: 'status_visita_familiar_frequentemente',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    grauParentescoFamiliarFrequentado: {
      field: 'grau_parentesco_familiar_frequentado',
      type: DataTypes.STRING,
      allowNull: true
    },
    higienePessoalSituacaoRua: {
      field: 'higiene_pessoal_situacao_rua',
      type: DataTypes.STRING,
      allowNull: true
    },
    statusTemAcessoHigienePessoalSituacaoRua: {
      field: 'status_tem_acesso_higiene_pessoal_situacao_rua',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING,
      allowNull: true
    },
    stExportado: {
      field: 'st_exportado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    fichaAtualizada: {
      field: 'ficha_atualizada',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nrProntuarioUbs: {
      field: 'nr_prontuario_ubs',
      type: DataTypes.BIGINT,
      allowNull: true
    },
    cpf: {
      type: DataTypes.STRING,
      allowNull: true
    },
    rg: {
      type: DataTypes.STRING,
      allowNull: true
    },
    tituloEleitor: {
      field: 'titulo_eleitor',
      type: DataTypes.STRING,
      allowNull: true
    },
    situacaoConjugal: {
      field: 'situacao_conjugal',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusEhFuncionarioPublico: {
      field: 'funcionario_publico',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusRecebeBolsaFamilia: {
      field: 'status_recebe_bolsa_familia',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusEhAlfabetizado: {
      field: 'alfabetizado',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    peso: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    altura: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusDesnutricaoGrave: {
      field: 'status_desnutricao_grave',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemDiarreia: {
      field: 'status_tem_diarreia',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    statusTemCancer: {
      field: 'status_tem_cancer',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipoTemCancer: {
      field: 'tipo_tem_cancer',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusTeveCancer: {
      field: 'status_teve_cancer',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipoTeveCancer: {
      field: 'tipo_teve_cancer',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipoDiabetes: {
      field: 'tipo_diabetes',
      type: DataTypes.INTEGER,
      allowNull: true
    },
    statusPraticaAtividadesFisicas: {
      field: 'status_pratica_atividades_fisicas',
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    familiaMudou: {
      field: 'familia_mudou',
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    cbo_profissional: {
      type: DataTypes.STRING,
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      field: 'date_created',
      type: DataTypes.DATE
    },
    updatedAt: {
      field: 'date_updated',
      type: DataTypes.DATE
    }
  }, {
    tableName: 'tb_cadastro_cidadao',
    timestamps: true
  });

  return CadastroIndividual;
};
