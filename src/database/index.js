const Sequelize = require('sequelize')
const dbConfig = require('../config/database')

const tb_cadastro_cidadao = require('../models/tb_cadastro_cidadao')
const tb_cadastro_domiciliar = require('../models/tb_cadastro_domiciliar')

const connection = new Sequelize(dbConfig)

tb_cadastro_cidadao.init(connection)
tb_cadastro_domiciliar.init(connection)

module.exports = connection