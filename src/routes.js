const express = require('express')
const UserControlller = require('./controllers/UserController')
const PatientControlller = require('./controllers/PatientController')
const AttendanceController = require('./controllers/AttendanceController')
const SessionController = require('./controllers/SessionController')



const routes = express.Router()

//rotas de autenticação
routes.get('/signin', SessionController.create)
routes.post('/signin', SessionController.store)


//rotas de usuarios
routes.get('/users', UserControlller.index)
routes.post('/users', UserControlller.store)
routes.put('/users/:user_id', UserControlller.update)
routes.delete('/users/:user_id', UserControlller.delete)


//rotas de pacientes
routes.get('/patients', PatientControlller.index)
routes.get('/patients/:patient_id', PatientControlller.show)
routes.post('/patients', PatientControlller.store)
routes.put('/patients/:patient_id', PatientControlller.update)
routes.delete('/patients/:patient_id', PatientControlller.delete)


//rotas de unidades atendimentos
routes.get('/attendances', AttendanceController.index)
routes.get('/attendances/:id/', AttendanceController.show)
routes.post('/patients/:patient_id/attendances', AttendanceController.store)
routes.put('/attendances/:id', AttendanceController.update)
routes.delete('/attendances/:id', AttendanceController.delete)

module.exports = routes

